﻿
namespace TakeHome1
{
    partial class windowGuessCoordinates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(windowGuessCoordinates));
            this.coordLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.goBackButton = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.resetButton = new System.Windows.Forms.Button();
            this.northWestButton = new System.Windows.Forms.Button();
            this.southWestButton = new System.Windows.Forms.Button();
            this.southEastButton = new System.Windows.Forms.Button();
            this.northEastButton = new System.Windows.Forms.Button();
            this.southButton = new System.Windows.Forms.Button();
            this.northButton = new System.Windows.Forms.Button();
            this.westButton = new System.Windows.Forms.Button();
            this.eastButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // coordLabel
            // 
            this.coordLabel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.coordLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.coordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.coordLabel.Location = new System.Drawing.Point(507, 110);
            this.coordLabel.Name = "coordLabel";
            this.coordLabel.Size = new System.Drawing.Size(291, 71);
            this.coordLabel.TabIndex = 40;
            this.coordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(341, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 71);
            this.label1.TabIndex = 39;
            this.label1.Text = "ESTIMATED POSITION";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label3.Location = new System.Drawing.Point(127, -1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(688, 81);
            this.label3.TabIndex = 41;
            this.label3.Text = "POSITION GUESSER";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // goBackButton
            // 
            this.goBackButton.Location = new System.Drawing.Point(829, 511);
            this.goBackButton.Margin = new System.Windows.Forms.Padding(4);
            this.goBackButton.Name = "goBackButton";
            this.goBackButton.Size = new System.Drawing.Size(100, 28);
            this.goBackButton.TabIndex = 42;
            this.goBackButton.Text = "Go back";
            this.goBackButton.UseVisualStyleBackColor = true;
            this.goBackButton.Click += new System.EventHandler(this.goBackButton_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::TakeHome1.Properties.Resources.directions;
            this.pictureBox2.Location = new System.Drawing.Point(172, 57);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(183, 174);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 44;
            this.pictureBox2.TabStop = false;
            // 
            // resetButton
            // 
            this.resetButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("resetButton.BackgroundImage")));
            this.resetButton.Location = new System.Drawing.Point(458, 339);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(86, 45);
            this.resetButton.TabIndex = 38;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // northWestButton
            // 
            this.northWestButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("northWestButton.BackgroundImage")));
            this.northWestButton.Location = new System.Drawing.Point(366, 288);
            this.northWestButton.Name = "northWestButton";
            this.northWestButton.Size = new System.Drawing.Size(86, 45);
            this.northWestButton.TabIndex = 37;
            this.northWestButton.Text = "Northwest";
            this.northWestButton.UseVisualStyleBackColor = true;
            this.northWestButton.Click += new System.EventHandler(this.northWestButton_Click);
            // 
            // southWestButton
            // 
            this.southWestButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("southWestButton.BackgroundImage")));
            this.southWestButton.Location = new System.Drawing.Point(366, 390);
            this.southWestButton.Name = "southWestButton";
            this.southWestButton.Size = new System.Drawing.Size(86, 45);
            this.southWestButton.TabIndex = 36;
            this.southWestButton.Text = "Southwest";
            this.southWestButton.UseVisualStyleBackColor = true;
            this.southWestButton.Click += new System.EventHandler(this.southWestButton_Click);
            // 
            // southEastButton
            // 
            this.southEastButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("southEastButton.BackgroundImage")));
            this.southEastButton.Location = new System.Drawing.Point(550, 390);
            this.southEastButton.Name = "southEastButton";
            this.southEastButton.Size = new System.Drawing.Size(86, 45);
            this.southEastButton.TabIndex = 35;
            this.southEastButton.Text = "Southeast";
            this.southEastButton.UseVisualStyleBackColor = true;
            this.southEastButton.Click += new System.EventHandler(this.southEastButton_Click);
            // 
            // northEastButton
            // 
            this.northEastButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("northEastButton.BackgroundImage")));
            this.northEastButton.Location = new System.Drawing.Point(550, 288);
            this.northEastButton.Name = "northEastButton";
            this.northEastButton.Size = new System.Drawing.Size(86, 45);
            this.northEastButton.TabIndex = 34;
            this.northEastButton.Text = "Northeast";
            this.northEastButton.UseVisualStyleBackColor = true;
            this.northEastButton.Click += new System.EventHandler(this.northEastButton_Click);
            // 
            // southButton
            // 
            this.southButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("southButton.BackgroundImage")));
            this.southButton.Location = new System.Drawing.Point(458, 390);
            this.southButton.Name = "southButton";
            this.southButton.Size = new System.Drawing.Size(86, 45);
            this.southButton.TabIndex = 33;
            this.southButton.Text = "South";
            this.southButton.UseVisualStyleBackColor = true;
            this.southButton.Click += new System.EventHandler(this.southButton_Click);
            // 
            // northButton
            // 
            this.northButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("northButton.BackgroundImage")));
            this.northButton.Location = new System.Drawing.Point(458, 288);
            this.northButton.Name = "northButton";
            this.northButton.Size = new System.Drawing.Size(86, 45);
            this.northButton.TabIndex = 32;
            this.northButton.Text = "North";
            this.northButton.UseVisualStyleBackColor = true;
            this.northButton.Click += new System.EventHandler(this.northButton_Click);
            // 
            // westButton
            // 
            this.westButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("westButton.BackgroundImage")));
            this.westButton.Location = new System.Drawing.Point(366, 339);
            this.westButton.Name = "westButton";
            this.westButton.Size = new System.Drawing.Size(86, 45);
            this.westButton.TabIndex = 31;
            this.westButton.Text = "West";
            this.westButton.UseVisualStyleBackColor = true;
            this.westButton.Click += new System.EventHandler(this.westButton_Click);
            // 
            // eastButton
            // 
            this.eastButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("eastButton.BackgroundImage")));
            this.eastButton.Location = new System.Drawing.Point(550, 339);
            this.eastButton.Name = "eastButton";
            this.eastButton.Size = new System.Drawing.Size(86, 45);
            this.eastButton.TabIndex = 30;
            this.eastButton.Text = "East";
            this.eastButton.UseVisualStyleBackColor = true;
            this.eastButton.Click += new System.EventHandler(this.eastButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::TakeHome1.Properties.Resources.arrows;
            this.pictureBox1.Location = new System.Drawing.Point(145, 220);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(688, 287);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // windowGuessCoordinates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CadetBlue;
            this.ClientSize = new System.Drawing.Size(942, 552);
            this.Controls.Add(this.goBackButton);
            this.Controls.Add(this.coordLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.northWestButton);
            this.Controls.Add(this.southWestButton);
            this.Controls.Add(this.southEastButton);
            this.Controls.Add(this.northEastButton);
            this.Controls.Add(this.southButton);
            this.Controls.Add(this.northButton);
            this.Controls.Add(this.westButton);
            this.Controls.Add(this.eastButton);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "windowGuessCoordinates";
            this.Text = "windowGuessCoordinates";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label coordLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Button northWestButton;
        private System.Windows.Forms.Button southWestButton;
        private System.Windows.Forms.Button southEastButton;
        private System.Windows.Forms.Button northEastButton;
        private System.Windows.Forms.Button southButton;
        private System.Windows.Forms.Button northButton;
        private System.Windows.Forms.Button westButton;
        private System.Windows.Forms.Button eastButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button goBackButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}