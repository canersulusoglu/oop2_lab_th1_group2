﻿namespace TakeHome1
{
    partial class windowApplications
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(windowApplications));
            this.label1 = new System.Windows.Forms.Label();
            this.calculatorAppBtn = new System.Windows.Forms.Button();
            this.cipherAppBtn = new System.Windows.Forms.Button();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.coordinateGuesserAppBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Impact", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.label1.Location = new System.Drawing.Point(100, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(595, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Welcome to the applications screen. More applications will be added soon.";
            // 
            // calculatorAppBtn
            // 
            this.calculatorAppBtn.BackColor = System.Drawing.Color.Transparent;
            this.calculatorAppBtn.BackgroundImage = global::TakeHome1.Properties.Resources.button2;
            this.calculatorAppBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.calculatorAppBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.calculatorAppBtn.FlatAppearance.BorderSize = 0;
            this.calculatorAppBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.calculatorAppBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.calculatorAppBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.calculatorAppBtn.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.calculatorAppBtn.Location = new System.Drawing.Point(41, 67);
            this.calculatorAppBtn.Margin = new System.Windows.Forms.Padding(0);
            this.calculatorAppBtn.Name = "calculatorAppBtn";
            this.calculatorAppBtn.Size = new System.Drawing.Size(156, 48);
            this.calculatorAppBtn.TabIndex = 17;
            this.calculatorAppBtn.Text = "Calculator";
            this.calculatorAppBtn.UseVisualStyleBackColor = false;
            this.calculatorAppBtn.Click += new System.EventHandler(this.calculatorAppBtn_Click);
            // 
            // cipherAppBtn
            // 
            this.cipherAppBtn.BackColor = System.Drawing.Color.Transparent;
            this.cipherAppBtn.BackgroundImage = global::TakeHome1.Properties.Resources.button2;
            this.cipherAppBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cipherAppBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cipherAppBtn.FlatAppearance.BorderSize = 0;
            this.cipherAppBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cipherAppBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cipherAppBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cipherAppBtn.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cipherAppBtn.Location = new System.Drawing.Point(601, 67);
            this.cipherAppBtn.Margin = new System.Windows.Forms.Padding(0);
            this.cipherAppBtn.Name = "cipherAppBtn";
            this.cipherAppBtn.Size = new System.Drawing.Size(156, 48);
            this.cipherAppBtn.TabIndex = 18;
            this.cipherAppBtn.Text = "Cipher";
            this.cipherAppBtn.UseVisualStyleBackColor = false;
            this.cipherAppBtn.Click += new System.EventHandler(this.cipherAppBtn_Click);
            // 
            // logoutBtn
            // 
            this.logoutBtn.BackColor = System.Drawing.Color.Transparent;
            this.logoutBtn.BackgroundImage = global::TakeHome1.Properties.Resources.button2;
            this.logoutBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.logoutBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.logoutBtn.FlatAppearance.BorderSize = 0;
            this.logoutBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.logoutBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.logoutBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logoutBtn.Font = new System.Drawing.Font("Segoe Print", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.logoutBtn.Location = new System.Drawing.Point(684, 308);
            this.logoutBtn.Margin = new System.Windows.Forms.Padding(0);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(107, 46);
            this.logoutBtn.TabIndex = 19;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = false;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.BackColor = System.Drawing.Color.Transparent;
            this.exitBtn.BackgroundImage = global::TakeHome1.Properties.Resources.button2;
            this.exitBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.exitBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.exitBtn.FlatAppearance.BorderSize = 0;
            this.exitBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.exitBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.exitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitBtn.Font = new System.Drawing.Font("Segoe Print", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.exitBtn.Location = new System.Drawing.Point(684, 354);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(0);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(107, 46);
            this.exitBtn.TabIndex = 20;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = false;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // coordinateGuesserAppBtn
            // 
            this.coordinateGuesserAppBtn.BackColor = System.Drawing.Color.Transparent;
            this.coordinateGuesserAppBtn.BackgroundImage = global::TakeHome1.Properties.Resources.button2;
            this.coordinateGuesserAppBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.coordinateGuesserAppBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.coordinateGuesserAppBtn.FlatAppearance.BorderSize = 0;
            this.coordinateGuesserAppBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.coordinateGuesserAppBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.coordinateGuesserAppBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.coordinateGuesserAppBtn.Font = new System.Drawing.Font("Segoe Print", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.coordinateGuesserAppBtn.Location = new System.Drawing.Point(41, 157);
            this.coordinateGuesserAppBtn.Margin = new System.Windows.Forms.Padding(0);
            this.coordinateGuesserAppBtn.Name = "coordinateGuesserAppBtn";
            this.coordinateGuesserAppBtn.Size = new System.Drawing.Size(156, 48);
            this.coordinateGuesserAppBtn.TabIndex = 21;
            this.coordinateGuesserAppBtn.Text = "Position Guess";
            this.coordinateGuesserAppBtn.UseVisualStyleBackColor = false;
            this.coordinateGuesserAppBtn.Click += new System.EventHandler(this.coordinateGuesserAppBtn_Click);
            // 
            // windowApplications
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::TakeHome1.Properties.Resources.esogu_den_ogrencilere_onemli_duyuru_92ad3;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 404);
            this.Controls.Add(this.coordinateGuesserAppBtn);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.logoutBtn);
            this.Controls.Add(this.cipherAppBtn);
            this.Controls.Add(this.calculatorAppBtn);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "windowApplications";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Applications screen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button calculatorAppBtn;
        private System.Windows.Forms.Button cipherAppBtn;
        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button coordinateGuesserAppBtn;
    }
}