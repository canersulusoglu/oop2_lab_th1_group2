﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeHome1
{
    public class CeaserCipher
    {
        private CeaserCipher()
        {

        }

        private static readonly object padlock = new object();
        private static CeaserCipher instance = null;

        public static CeaserCipher Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new CeaserCipher();
                    }
                    return instance;
                }
            }
        }

        private int mod(int x, int m)
        {
            return (x % m + m) % m;
        }

        public string Encipher(string input, int key, string alphabet)
        {
            string result = "";
            string lowerCaseInput = input.ToLower();
            string lowerCaseAlphabet = alphabet.ToLower();
            StringBuilder tmp = new StringBuilder(lowerCaseInput);
            HashSet<int> set = new HashSet<int>();

            key = Math.Abs(key);

            for (int i = 0; i < lowerCaseAlphabet.Length; i++)
            {
                char ch = lowerCaseAlphabet[i];

                for (int j = 0; j < tmp.Length; j++)
                {
                    if (tmp[j] == ch)
                    {

                        if (!set.Contains(j))
                        {
                            if (i + key > alphabet.Length - 1)
                            {
                                int a = alphabet.Length - i;
                                tmp[j] = alphabet[mod((key - a), alphabet.Length)];
                            }
                            else
                            {
                                tmp[j] = alphabet[i + key];
                            }
                            set.Add(j);
                        }

                    }
                }

            }
            result = tmp.ToString();

            return result;
        }

        public string Decipher(string input, int key, string alphabet)
        {
            string result = "";
            string lowerCaseInput = input.ToLower();
            string lowerCaseAlphabet = alphabet.ToLower();
            StringBuilder tmp = new StringBuilder(lowerCaseInput);
            HashSet<int> set = new HashSet<int>();

            key = Math.Abs(key);

            for (int i = 0; i < lowerCaseAlphabet.Length; i++)
            {
                char ch = lowerCaseAlphabet[i];
                for (int j = 0; j < tmp.Length; j++)
                {
                    if (tmp[j] == ch)
                    {
                        if (!set.Contains(j))
                        {
                            if (i - key < 0)
                            {
                                int a = key - i;
                                tmp[j] = alphabet[mod((alphabet.Length - a), alphabet.Length)];
                            }
                            else
                            {
                                tmp[j] = alphabet[i - key];
                            }
                            set.Add(j);
                        }

                    }
                }
            }
            result = tmp.ToString();

            return result;
        }
    }
}
