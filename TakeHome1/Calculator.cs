﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeHome1
{
    class Calculator
    {
        /*
         * This method adds two number.
         * @param float number1
         * @param float number2
        */
        public static float Addition(float number1, float number2)
        {
            return (number1 + number2);
        }

        /*
         * This method subtracts the second from the first number.
         * @param float number1
         * @param float number2
        */
        public static float Subtraction(float number1, float number2)
        {
            return (number1 - number2);
        }

        /*
         * This method multiplies two numbers.
         * @param float number1
         * @param float number2
        */
        public static float Multiplication(float number1, float number2)
        {
            return (number1 * number2);
        }

        /*
         * This method divides the first number by the second number.
         * @param float number1
         * @param float number2
        */
        public static float Division(float number1, float number2)
        {
            if (number2 == 0)
            {
                throw new DivideByZeroException();
            }
            else
            {
                return (number1 / number2);
            }
        }
    }
}
