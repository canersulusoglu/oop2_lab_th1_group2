﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeHome1
{
    public partial class windowCipher : Form
    {
        public windowCipher()
        {
            InitializeComponent();
        }

        private void txt_TextChanged(object sender, EventArgs e)
        {
            int number;
            bool result = radioButton3.Checked ? Int32.TryParse(txtValue.Text, out number) : CheckAlphabet();
            
            if (!result)
            {
                string message = "";
                message = (txtValue.Text == "") ? "Value part can not be empty" : (radioButton3.Checked ?"Please enter an integer value" :"The character you input doesn't exist in the alphabet");
                MessageBox.Show(message);
            }
            else
            {
                string message = "";
                message = radioButton3.Checked ? (radioButton1.Checked ? CeaserCipher.Instance.Encipher(textBox.Text, Int32.Parse(txtValue.Text), txtAlphabet.Text) : CeaserCipher.Instance.Decipher(textBox.Text, Int32.Parse(txtValue.Text), txtAlphabet.Text)) : (radioButton1.Checked ? VigenereCipher.Instance.Encryption(textBox.Text, txtValue.Text) : VigenereCipher.Instance.Decryption(textBox.Text, txtValue.Text));
                lblResult.Text = message;
            }
        }

        private bool CheckAlphabet()
        {
            bool result = false;
            for (int i = 0; i < txtValue.TextLength; i++)
            {
                result = false;
                for (int j = 0; j < txtAlphabet.TextLength; j++)
                {
                    if (txtValue.Text[i] == txtAlphabet.Text[j])
                    {
                        result = true;
                        break;
                    }                       
                }
            }

            return result;
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {

            int number;
            bool result = radioButton3.Checked ? Int32.TryParse(txtValue.Text, out number) : CheckAlphabet();
            
            if (!result)
            {
                string message = "";
                message = (txtValue.Text == "") ? "Value part can not be empty" : (radioButton3.Checked ?"Please enter an integer value" :"The character you input doesn't exist in the alphabet");
                MessageBox.Show(message);
            }
            else
            {
                string message = "";
                message = radioButton3.Checked ? (radioButton1.Checked ? CeaserCipher.Instance.Encipher(textBox.Text, Int32.Parse(txtValue.Text), txtAlphabet.Text) : CeaserCipher.Instance.Decipher(textBox.Text, Int32.Parse(txtValue.Text), txtAlphabet.Text)) : (radioButton1.Checked ? VigenereCipher.Instance.Encryption(textBox.Text, txtValue.Text) : VigenereCipher.Instance.Decryption(textBox.Text, txtValue.Text));
                lblResult.Text = message;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult x = MessageBox.Show("Do you want to go back to application screen?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (x == DialogResult.Yes)
            {
                this.Hide();
                windowApplications applications = new windowApplications();
                applications.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult x = MessageBox.Show("Do you want to exit?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            if (x == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
