﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeHome1
{
    class VigenereCipher
    {
        private static VigenereCipher instance = null;
        private static readonly object padlock = new object();
        VigenereCipher() { }
        public static VigenereCipher Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null) { instance = new VigenereCipher(); }
                    return instance;
                }
            }
        }
        int keyshiftvalue = 1;
        private string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public string Alphabet
        {
            get { return this.alphabet; }
            set { this.alphabet = value; }
        }

        public string Encryption(string String, string key)
        {
            string[] s = new string[String.Length];
            for (int i = 0; i < String.Length; i++)
            {
                int key_shift = 0, current_key = 0;//these will show us how many times we need to shift for each key
                for (int j = 0; j < alphabet.Length; j++)
                {
                    if (String[i].ToString() == alphabet[j].ToString()) { current_key = j; }

                    if (key[i % key.Length].ToString() == alphabet[j].ToString()) { key_shift = (j + 1) * keyshiftvalue; }
                }
                s[i] = alphabet[(alphabet.Length + current_key + key_shift) % alphabet.Length].ToString();
            }
            keyshiftvalue = 1;
            return string.Concat(s);
        }

        public string Decryption(string a, string key) //decrypting with a known key
        {
            keyshiftvalue = -1;
            return Encryption(a, key);
        }
    }
}