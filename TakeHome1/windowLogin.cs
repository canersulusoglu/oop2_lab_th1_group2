﻿using TakeHome1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace TakeHome1
{
    public partial class windowLogin : Form
    {
        Timer timer = new Timer();
        int timeLeft = 3;
        public windowLogin()
        {
            InitializeComponent();
            label1.Location = pictureBox1.PointToClient(label1.Parent.PointToScreen(label1.Location));
            label1.Parent = pictureBox1;
            label2.Location = pictureBox1.PointToClient(label2.Parent.PointToScreen(label2.Location));
            label2.Parent = pictureBox1;

        }

        private void pictureBox8_MouseDown(object sender, MouseEventArgs e)
        {
            passwordText.UseSystemPasswordChar = false;
        }

        private void pictureBox8_MouseUp(object sender, MouseEventArgs e)
        {
            passwordText.UseSystemPasswordChar = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void onLoginClick(object sender, MouseEventArgs e)
        {

            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] dizi = Encoding.UTF8.GetBytes(passwordText.Text);
            dizi = md5.ComputeHash(dizi);
            StringBuilder sb = new StringBuilder();
            foreach (byte ba in dizi)
            {
                sb.Append(ba.ToString("x2").ToLower());
            }
            string md5Password = sb.ToString();

            bool loginIsSuccessful = Database.Instance.UserExists(userNameText.Text, md5Password);

            successLabel.Visible = true;
            if (loginIsSuccessful)
            {
                successLabel.Text = "Logging In...";
                successLabel.Left = 550;
                successLabel.ForeColor = Color.Green;

                timer.Interval = 1000;
                timer.Enabled = true;

                timer.Tick += timer_Tick;
                lblCountDown.Text = timeLeft.ToString();


                timer.Start();

            }
            else
            {
                successLabel.Text = "Wrong User Information! - Try Again";
                successLabel.Left = 510;
                successLabel.ForeColor = Color.Red;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            lblCountDown.Text = timeLeft.ToString();
            lblCountDown.Visible = true;

            timeLeft -= 1;

            if (timeLeft < 0)
            {
                this.Hide();
                windowApplications applications = new windowApplications();
                applications.FormClosed += (s, args) =>
                {
                    this.Close();
                };
                applications.Show();
                timer.Stop();

            }
        }

        private void bntSignUp_Click(object sender, EventArgs e)
        {
            windowSignUp signUp = new windowSignUp();
            signUp.ShowDialog();
        }
    }

}

