﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeHome1
{
    public partial class windowGuessCoordinates : Form
    {
        positionGuesser pg = new positionGuesser();

        public windowGuessCoordinates()
        {
            InitializeComponent();
        }

        private void windowGuessCoordinates_Load(object sender, EventArgs e)
        {
            coordLabel.Text = pg.printPosition();
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            pg.ResetPosition();
            coordLabel.Text = pg.printPosition();
        }

        private void northButton_Click(object sender, EventArgs e)
        {
            pg.MakeGuess(Direction.North);
            coordLabel.Text = pg.printPosition();
        }

        private void northEastButton_Click(object sender, EventArgs e)
        {
            pg.MakeGuess(Direction.NorthEast);
            coordLabel.Text = pg.printPosition();
        }

        private void eastButton_Click(object sender, EventArgs e)
        {
            pg.MakeGuess(Direction.East);
            coordLabel.Text = pg.printPosition();
        }

        private void southEastButton_Click(object sender, EventArgs e)
        {
            pg.MakeGuess(Direction.SouthEast);
            coordLabel.Text = pg.printPosition();
        }

        private void southButton_Click(object sender, EventArgs e)
        {
            pg.MakeGuess(Direction.South);
            coordLabel.Text = pg.printPosition();
        }

        private void southWestButton_Click(object sender, EventArgs e)
        {
            pg.MakeGuess(Direction.SouthWest);
            coordLabel.Text = pg.printPosition();
        }

        private void westButton_Click(object sender, EventArgs e)
        {
            pg.MakeGuess(Direction.West);
            coordLabel.Text = pg.printPosition();
        }

        private void northWestButton_Click(object sender, EventArgs e)
        {
            pg.MakeGuess(Direction.NorthWest);
            coordLabel.Text = pg.printPosition();
        }

        private void goBackButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
