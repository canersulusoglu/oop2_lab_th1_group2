﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeHome1
{
    enum Direction
    {
        North,
        NorthEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West,
        NorthWest
    }

    class positionGuesser
    {
        // Map size AxA
        private int mapSize = 1024;

        // coordinates
        private int x = 0;
        private int y = 0;

        // Limits by coordinates
        private int limitUpper;
        private int limitLower;
        private int limitRight;
        private int limitLeft;

        public positionGuesser()
        {
            this.ResetPosition();
        }

        public int X { get { return this.x; } }
        public int Y { get { return this.y; } }

        public void ResetPosition()
        {
            this.x = 0;
            this.y = 0;
            this.limitUpper = (mapSize / 2);
            this.limitLower = -(mapSize / 2);
            this.limitRight = (mapSize / 2);
            this.limitLeft = -(mapSize / 2);
        }

        public string printPosition()
        {
            return "(" + this.x + "," + this.y + ")";
        }

        public void MakeGuess(Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    NorthGuess();
                    break;
                case Direction.NorthEast:
                    NorthEastGuess();
                    break;
                case Direction.East:
                    EastGuess();
                    break;
                case Direction.SouthEast:
                    SouthEastGuess();
                    break;
                case Direction.South:
                    SouthGuess();
                    break;
                case Direction.SouthWest:
                    SouthWestGuess();
                    break;
                case Direction.West:
                    WestGuess();
                    break;
                case Direction.NorthWest:
                    NorthWestGuess();
                    break;
                default:
                    break;
            }
        }

        private void NorthGuess()
        {
            // Determine the limits
            limitLower = y;

            // x doesnt change
            // Random Number for y axis
            y = RandomNumbers(y, limitUpper);
        }

        private void NorthEastGuess()
        {
            // Determine the limits
            limitLower = y;
            limitLeft = x;

            // Random Number for x axis
            x = RandomNumbers(x, limitRight);
            // Random Number for y axis
            y = RandomNumbers(y, limitUpper);
        }

        private void EastGuess()
        {
            // Determine the limits
            limitLeft = x;

            // Random Number for x axis
            x = RandomNumbers(x, limitRight);
            // y doesnt change
        }

        private void SouthEastGuess()
        {
            // Determine the limits
            limitUpper = y;
            limitLeft = x;

            // Random Number for x axis
            x = RandomNumbers(x, limitRight);
            // Random Number for y axis
            y = RandomNumbers(y, limitLower);
        }

        private void SouthGuess()
        {
            // Determine the limits
            limitUpper = y;

            // x doesnt change
            // Random Number for y axis
            y = RandomNumbers(y, limitLower);
        }

        private void SouthWestGuess()
        {
            // Determine the limits
            limitUpper = y;
            limitRight = x;

            // Random Number for x axis
            x = RandomNumbers(x, limitLeft);
            // Random Number for y axis
            y = RandomNumbers(y, limitLower);
        }

        private void WestGuess()
        {
            // Determine the limits
            limitRight = x;

            // Random Number for x axis
            x = RandomNumbers(x, limitLeft);
            // y doesnt change
        }

        private void NorthWestGuess()
        {
            // Determine the limits
            limitLower = y;
            limitRight = x;

            // Random Number for x axis
            x = RandomNumbers(x, limitLeft);
            // Random Number for y axis
            y = RandomNumbers(y, limitUpper);
        }

        private int RandomNumbers(int start, int end)
        {
            if (start > end)
            {
                Random random = new Random();
                int randomInteger = random.Next(end, start + 1);
                return randomInteger;
            }
            else
            {
                Random random = new Random();
                int randomInteger = random.Next(start, end + 1);
                return randomInteger;
            }
        }
    }
}
