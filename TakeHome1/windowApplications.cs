﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeHome1
{
    public partial class windowApplications : Form
    {
        public windowApplications()
        {
            InitializeComponent();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            DialogResult x = MessageBox.Show("Do you want to exit?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            if (x == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            DialogResult x = MessageBox.Show("Do you want to log out?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (x == DialogResult.Yes)
            {
                this.Hide();
                windowLogin Windowlogin = new windowLogin();
                Windowlogin.Show();
            }
        }

        private void calculatorAppBtn_Click(object sender, EventArgs e)
        {
            windowCalc Calc = new windowCalc();
            Calc.Show();
        }

        private void cipherAppBtn_Click(object sender, EventArgs e)
        {
            windowCipher Cipher = new windowCipher();
            Cipher.Show();
        }

        private void coordinateGuesserAppBtn_Click(object sender, EventArgs e)
        {
            windowGuessCoordinates guessCoord = new windowGuessCoordinates();
            guessCoord.Show();
        }
    }
}
