﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeHome1
{
    public class Database
    {
        private Database()
        {
            
        }

        private static readonly object padlock = new object();
        private static Database instance = null;
        private List<User> users = new List<User>();

        public static Database Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Database();
                    }
                    return instance;
                }
            }
        }

        public bool AddUser(User user)
        {
            foreach (User u in users)
            {
                if (u.UserName == user.UserName)
                {
                    return false;
                }
            }
            users.Add(user);
            return true;
        }

        public bool UserExists(string userName, string md5Password)
        {
            foreach (User u in users)
            {
                if (u.UserName == userName && u.Password == md5Password)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
