﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeHome1
{
    public partial class windowCalc : Form
    {
        public windowCalc()
        {
            InitializeComponent();
        }
        float num1 = 0, num2 = 0;
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.BackColor = Color.Blue;
            button2.BackColor = Color.White;
            button3.BackColor = Color.White;
            button4.BackColor = Color.White;
            if (textBox1.TextLength > 0)
            {
                num1 = float.Parse(textBox1.Text);
            }
            if (textBox2.TextLength > 0)
            {
                num2 = float.Parse(textBox2.Text);
            }
            try
            {
                float result = Calculator.Addition(num1, num2);
                Result.Text = result.ToString();
                if (result > 0)
                {
                    Result.BackColor = Color.Green;
                }
                else
                {
                    Result.BackColor = Color.Red;
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error"); }
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            button1.ForeColor = Color.Red;
        }

        private void button2_MouseHover(object sender, EventArgs e)
        {
            button2.ForeColor = Color.Red;
        }

        private void button3_MouseHover(object sender, EventArgs e)
        {
            button3.ForeColor = Color.Red;
        }

        private void button4_MouseHover(object sender, EventArgs e)
        {
            button4.ForeColor = Color.Red;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
            button2.BackColor = Color.Blue;
            button3.BackColor = Color.White;
            button4.BackColor = Color.White;
            if (textBox1.TextLength > 0)
            {
                num1 = float.Parse(textBox1.Text);
            }
            if (textBox2.TextLength > 0)
            {
                num2 = float.Parse(textBox2.Text);
            }
            try
            {
                float result = Calculator.Subtraction(num1, num2);
                Result.Text = result.ToString();
                if (result > 0)
                {
                    Result.BackColor = Color.Green;
                }
                else
                {
                    Result.BackColor = Color.Red;
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error"); }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
            button2.BackColor = Color.White;
            button3.BackColor = Color.Blue;
            button4.BackColor = Color.White;
            if (textBox1.TextLength > 0)
            {
                num1 = float.Parse(textBox1.Text);
            }
            if (textBox2.TextLength > 0)
            {
                num2 = float.Parse(textBox2.Text);
            }
            try
            {
                float result = Calculator.Multiplication(num1, num2);
                Result.Text = result.ToString();
                if (result > 0)
                {
                    Result.BackColor = Color.Green;
                }
                else
                {
                    Result.BackColor = Color.Red;
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error"); }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
            button2.BackColor = Color.White;
            button3.BackColor = Color.White;
            button4.BackColor = Color.Blue;
            if (textBox1.TextLength > 0)
            {
                num1 = float.Parse(textBox1.Text);
            }
            if (textBox2.TextLength > 0)
            {
                num2 = float.Parse(textBox2.Text);
            }
            try
            {
                float result = Calculator.Division(num1, num2);
                Result.Text = result.ToString();
                if (result > 0)
                {
                    Result.BackColor = Color.Green;
                }
                else
                {
                    Result.BackColor = Color.Red;
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error"); }
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            //int num1=0;
            //num1 = int.Parse(textBox1.Text);
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            //num1 = int.Parse(textBox1.Text);
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.ForeColor = Color.Black;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.ForeColor = Color.Black;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.ForeColor = Color.Black;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            button4.ForeColor = Color.Black;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == ',' || e.KeyChar == '-' || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == ',' || e.KeyChar == '-' || char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            //int num2 = 0;
            //num2 = int.Parse(textBox2.Text);
        }
    }
}
