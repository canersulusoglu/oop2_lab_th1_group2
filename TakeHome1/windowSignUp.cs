﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Windows.Forms;

namespace TakeHome1
{
    public partial class windowSignUp : Form
    {
        public windowSignUp()
        {
            InitializeComponent();
        }

        private void windowSignUp_Load(object sender, EventArgs e)
        {

        }

        private void signUpButton_Click(object sender, EventArgs e)
        {
            messageLabel.ForeColor = Color.Red;
            messageLabel.Text = "Username must be more 6 character.";
            if (usernameText.TextLength < 6)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Username must be more 6 character.";
            }
            else if (passwordText.TextLength < 6)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Password must be more 6 character.";
            }
            else if (confirmPasswordText.Text != passwordText.Text)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Password and confirm password must match.";
            }
            else
            {
                // MD5 Hash Password
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                byte[] dizi = Encoding.UTF8.GetBytes(confirmPasswordText.Text);
                dizi = md5.ComputeHash(dizi);
                StringBuilder sb = new StringBuilder();
                foreach (byte ba in dizi)
                {
                    sb.Append(ba.ToString("x2").ToLower());
                }
                string md5Password = sb.ToString();
                
                bool result = Database.Instance.AddUser(new User(usernameText.Text, md5Password));
                if (result)
                {
                    messageLabel.ForeColor = Color.Green;
                    messageLabel.Text = "Successfully, you signed up.";
                }
                else
                {
                    messageLabel.ForeColor = Color.Red;
                    messageLabel.Text = "This username is used by someone else.";
                }
                
            }
        }

        private void goBackButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox8_MouseDown(object sender, MouseEventArgs e)
        {
            passwordText.UseSystemPasswordChar = false;
        }

        private void pictureBox8_MouseUp(object sender, MouseEventArgs e)
        {
            passwordText.UseSystemPasswordChar = true;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            confirmPasswordText.UseSystemPasswordChar = false;
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            confirmPasswordText.UseSystemPasswordChar = true;
        }
    }
}
